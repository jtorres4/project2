
#include "pipelined-cpu.h"
#include "memory.h"
#include "syscall.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

uint32_t toLittleEndian (uint32_t num);

int main( int argc, char *argv[] )
{
    FILE *f;
	struct control_unit control_ToD;
    struct control_unit control_ToX;
    struct control_unit control_ToM;
    struct control_unit control_ToW;

	//struct ALU_control ALU_controler; - integrated into control_unit^

	struct IF_ID_buffer if_id;
	struct ID_EX_buffer id_ex;
	struct EX_MEM_buffer ex_mem;
	struct MEM_WB_buffer mem_wb;
	int i;
    size_t count;

  /* Set the PC */
	cpu_ctx.PC = 0x00400000;

	/* Initialize register to 0 */
	for ( i = 0; i < 32; i++ ) {
		cpu_ctx.GPR[i] = 0;
	}

  /* Set the SP */
  // Chaned Stack Pointer to 2 (from 29), regs assumed to grow upwards
  	cpu_ctx.GPR[2] = 0x10002000;

	/* Read memory from the input file */
	f = fopen(argv[1], "r");
	assert(f);
  	count = fread(data_memory, sizeof(uint32_t), 1024, f);
  	assert(count == 1024);
  	count = fread(instruction_memory, sizeof(uint32_t), 1024, f);
  	assert(count == 1024);
	fclose(f);


	float instructions = 0;
	float cycles = 0;
	int ending = 0;
	int stalls = 0;
	int squashes = 0;

	//Assume empty sections of pipeline -> stalled
	//This means we will always have atleast four stalls in every pipeline
	control_ToW.stall = 1;
	control_ToM.stall = 1;
	control_ToX.stall = 1;
	control_ToD.stall = 1;

	//Change endian-ness of memory
	for (int i = 0; i < 1024; i++){
		//printf("x%x ", data_memory[i]);
		data_memory[i] = toLittleEndian(data_memory[i]);
		//printf("x%x ", data_memory[i]);
	}
	printf("\n");

	while(ending != 4) {

        //Stages are inverted so the stage N can clear, so that stage N - 1 can write for it
		if(control_ToW.stall != 1){
			//printf("Writeback! ");
        	writeback(&mem_wb, &control_ToW);
		} else{
			printf("WB Stalled! ");
			stalls++;
		}

		if(control_ToM.stall != 1){
			//printf("Memory! ");
        	control_ToW = memory( &ex_mem, &mem_wb, &control_ToM);
		}
		else{
			printf("Mem Stalled! ");
			control_ToW.stall = 1;
			//stalls++;
		}

		if(control_ToX.stall != 1){
			//printf("Execute! ");
        	control_ToM = execute( &id_ex, &ex_mem, &control_ToX);
			if(control_ToM.loadUseStall == 1){
				//printf("Load use stall!");
				stalls++;
			}
		}
		else{
			printf("Ex Stalled! ");
			control_ToM.stall = 1;
			//stalls++;
		}
		if ( cpu_ctx.PC == 0 ) break;

		if(control_ToD.stall != 1){
			//printf("Decode! ");
        	control_ToX = decode( &if_id, &id_ex, &control_ToD);
			if(control_ToX.squash == 1){
				///instructions++; -- Bad instruction
				//printf("Squashed instruction! PC = %x, %d \n", cpu_ctx.PC, ((cpu_ctx.PC -  0x00400000)/4));
				squashes++;
				//printf("Next intrs - %d",instruction_memory[(cpu_ctx.PC -  0x00400000)/4]);
			}
			if(control_ToX.decodeStall == 1){
				//printf("Decode stall!");
				stalls++;
			}
		}else{
			printf("Dec Stalled! ");
			control_ToX.stall = 1;
			//stalls++;
		}
		if ( cpu_ctx.PC == 0 ) break;

		if(instruction_memory[(cpu_ctx.PC -  0x00400000)/4] != 0){
			//printf("Fetch! ");
        	control_ToD = fetch( &if_id);
			instructions += 1;
			ending = 0;
		}else{
			printf("Out of instructions to fetch");
			control_ToD.stall = 1;
			ending++;
			//stalls++;
		}

		printf("\n\n");
		if ( cpu_ctx.PC == 0 ) break;
	}

	//Calculate cycles, squashes and instructions
	//Will NOT print if forcefully exited by ecall
	cycles = stalls + squashes + instructions;
	printf("Regs at end of call, after %5.0f good instuctions and %5.0f cycles \n", instructions, cycles);
	printf("Total stalls: %d  and %d squashes \n", stalls, squashes);
	float cpi = cycles/instructions; 
	printf("Total CPI = %0.4f\n", cpi);
	for(int i = 0; i < 32; i++){
		printf(" Reg-%2d Val-0x%08x ||", i, cpu_ctx.GPR[i]);
		if((i+1) % 4 == 0 && i != 0){printf("\n");}
	} printf("\n");

	return 0;
}

uint32_t toLittleEndian (uint32_t num){
    // separate bytes from number
    uint32_t byteW = num & 0x000000FF;
    uint32_t byteX = num & 0x0000FF00;
    uint32_t byteY = num & 0x00FF0000;
    uint32_t byteZ = num & 0xFF000000;

    // shift each byte to LSB position
    byteW >>= 0;
    byteX >>= 8;
    byteY >>= 16;
    byteZ >>= 24;

    // now shift each byte to little endian position
    byteW <<= 24;
    byteX <<= 16;
    byteY <<= 8;
    byteZ <<= 0;

    // combine bytes
    num = (byteW | byteX | byteY | byteZ);
    return num;
}