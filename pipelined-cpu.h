#include <stdint.h>

struct cpu_context {
	uint32_t PC;
	uint32_t GPR[32];
};

extern struct cpu_context cpu_ctx;

struct control_unit{
	int stall; 		//Stalls -> Wait
	int loadUseStall; //Add a stall from Load/USe
	int decodeStall; //Add a stall in decode branch
	int squash;		//Squash -> Do Nothing
	int writeReg; 	//Where to write register
	int branch;   	//Branch control signal. Do I branch?
	int MemRead; 	//Read input from mem
	int MemToReg;  	//Send mem to reg (LW)
	int ALUop0;		//Branch Op?
	int ALUop1;   	//Regular ALU op for determining add/and/or/sub
	int MemWrite;	//Do I store to mem?
	int ALUsrc; 	//Do i get second operand from imm or reg?
	int PCsrc;      //If 0, pc +=4, else get branch from ALU
    int ALUControl; //What does ALU do?
};

struct forward_unit{
	int aluForwardReg;
	int aluForwardData;

	int memForwardReg;
	int memForwardData;
	int loadUseHazard;

	int wbForwardReg;
	int wbForwardData;
};

extern struct forward_unit forward;

struct IF_ID_buffer {
	uint32_t instruction;
	uint32_t pc;
	uint32_t next_pc;
};

struct ID_EX_buffer {
	uint32_t opcode;
	uint32_t funct3;
	uint32_t funct7;
	int regOne;
	int regTwo;
	int regOneData;
	int regTwoData;
	int immediate;
};

struct EX_MEM_buffer {
	int ALUresult;
	int StoreData;
};

struct MEM_WB_buffer {
	uint32_t resultToWB;
};

struct control_unit fetch( struct IF_ID_buffer *out);
struct control_unit decode( struct IF_ID_buffer *in, struct ID_EX_buffer *out, struct control_unit *control);
struct control_unit execute( struct ID_EX_buffer *in, struct EX_MEM_buffer *out, struct control_unit *control);
struct control_unit memory( struct EX_MEM_buffer *in, struct MEM_WB_buffer *out, struct control_unit *control);
int writeback( struct MEM_WB_buffer *in, struct control_unit *control );


int ALUSet(int funct3, int funct7, struct control_unit *controler);
int runALU(int dataOne, int dataTwo, int ALUControl);
int mux(int a, int b, int c);