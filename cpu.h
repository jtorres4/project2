/*
 * Copyright (C) 2018 Gedare Bloom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdint.h>

struct cpu_context {
	uint32_t PC;
	uint32_t GPR[32];
};

extern struct cpu_context cpu_ctx;

struct control_unit{
	int writeReg; 	//Where to write register
	int branch;   	//Branch control signal. Do I branch?
	int MemRead; 	//Read input from mem
	int MemToReg;  	//Send mem to reg (LW)
	int ALUop0;		//Branch Op?
	int ALUop1;   	//Regular ALU op for determining add/and/or/sub
	int MemWrite;	//Do I store to mem?
	int ALUsrc; 	//Do i get second operand from imm or reg?
	int PCsrc;      //If 0, pc +=4, else get branch from ALU
};

struct ALU_control{
	int ALUControl;
};

struct IF_ID_buffer {
	uint32_t instruction;
	uint32_t pc;
	uint32_t next_pc;
};

struct ID_EX_buffer {
	uint32_t opcode;
	uint32_t funct3;
	uint32_t funct7;
	int regOneData;
	int regTwoData;
	int immediate;
};

struct EX_MEM_buffer {
	int ALUresult;
	int StoreData;
};

struct MEM_WB_buffer {
	uint32_t resultToWB;
};

int fetch( struct IF_ID_buffer *out);
int decode( struct IF_ID_buffer *in, struct ID_EX_buffer *out, struct control_unit *control);
int execute( struct ID_EX_buffer *in, struct EX_MEM_buffer *out, struct control_unit *control, struct ALU_control *ALU_controler);
int memory( struct EX_MEM_buffer *in, struct MEM_WB_buffer *out, struct control_unit *control);
int writeback( struct MEM_WB_buffer *in, struct control_unit *control );


void ALUSet(int funct3, int funct7, struct control_unit *controler, struct ALU_control *ALU_controler);
int runALU(int dataOne, int dataTwo, struct ALU_control *ALU_controler);
int mux(int a, int b, int c);
