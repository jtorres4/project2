

all: single-cycle-mis pipeline-mis

single-cycle-mis: memory.c memory.h cpu.c cpu.h syscall.c syscall.h single-cycle.c
					gcc -O2 memory.c cpu.c syscall.c single-cycle.c -o single-cycle-mis

pipeline-mis: memory.c memory.h pipelined-cpu.c pipelined-cpu.h syscall.c syscall.h pipeline.c
					gcc -O2 memory.c pipelined-cpu.c syscall.c pipeline.c -o pipeline-mis

clean:
	rm single-cycle-mis

