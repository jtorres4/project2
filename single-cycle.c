/*
 * Copyright (C) 2018 Gedare Bloom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu.h"
#include "memory.h"
#include "syscall.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#define DEBUG

uint32_t toLittleEndian (uint32_t num);

int main( int argc, char *argv[] )
{
	FILE *f;
	struct control_unit control;
	struct ALU_control ALU_controler;
	struct IF_ID_buffer if_id;
	struct ID_EX_buffer id_ex;
	struct EX_MEM_buffer ex_mem;
	struct MEM_WB_buffer mem_wb;
	int i;
    size_t count;

  /* Set the PC */
	cpu_ctx.PC = 0x00400000;

	/* Initialize register to 0 */
	for ( i = 0; i < 32; i++ ) {
		cpu_ctx.GPR[i] = 0;
	}

  /* Set the SP */
  // Chaned Stack Pointer to 2 (from 29), regs assumed to grow upwards
  	cpu_ctx.GPR[2] = 0x10002000;

	/* Read memory from the input file */
	f = fopen(argv[1], "r");
	assert(f);
  	count = fread(data_memory, sizeof(uint32_t), 1024, f);
  	assert(count == 1024);
  	count = fread(instruction_memory, sizeof(uint32_t), 1024, f);
  	assert(count == 1024);
	fclose(f);

	//Test example we started with
	//addi    a0, zero, 1		a0 = 1   		[0000 0000 0001] [0000 0] [000] [0101 0] [001 0011]
	//addi    a1, zero, 2		a1 = 2			[0000 0000 0010] [0000 0] [000] [0101 0] [001 0011]
	//add     a2, a1, a0		a2 = 1 + 2 = 3  [0000 000] [0 1010] [0101 1] [000] [0110 0] [011 0011]
	//uint32_t testArray[3] = {0b00000000000100000000010100010011, 0b00000000001000000000010100010011, 0b00000000101001011000011000110011};

	int counter = 0;

	//Changes endianness of memory to big endian
	for (int i = 0; i < 1024; i++){
		//printf("x%x", data_memory[i]);
		data_memory[i] = toLittleEndian(data_memory[i]);
		//printf("x%x", data_memory[i]);
	}
	printf("\n");

	while(instruction_memory[(cpu_ctx.PC -  0x00400000)/4] != 0) {

//Movied to fetch stage
#if defined(DEBUG)
		//printf("FETCH from PC=%x\n", cpu_ctx.PC);
#endif

		fetch( &if_id);

		//printf("Decoding... \n");
		decode( &if_id, &id_ex, &control);

		//printf("Executing... \n");
		execute( &id_ex, &ex_mem, &control, &ALU_controler);

		//printf("Memory... \n");
		memory( &ex_mem, &mem_wb, &control);

		//printf("Writeback... \n");
		writeback(&mem_wb, &control);

		printf("\n");
		counter += 1;
		if ( cpu_ctx.PC == 0 ) break;
	}

	//Instructions and end of program registers
	//Will NOT print if forcefully exited by ecall
	printf("Regs at end of call, after %d instuctions \n", counter);
	for(int i = 0; i < 32; i++){
		printf(" Reg-%2d Val-0x%08x ||", i, cpu_ctx.GPR[i]);
		if((i+1) % 4 == 0 && i != 0){printf("\n");}
	} printf("\n");

	//printf("Data in 2047 %d \n", data_memory[2047]);

	//printf("Data in reg 28 is %d \n",cpu_ctx.GPR[28]);

	return 0;
}

uint32_t toLittleEndian (uint32_t num){
    // separate bytes from number
    uint32_t byteW = num & 0x000000FF;
    uint32_t byteX = num & 0x0000FF00;
    uint32_t byteY = num & 0x00FF0000;
    uint32_t byteZ = num & 0xFF000000;

    // shift each byte to LSB position
    byteW >>= 0;
    byteX >>= 8;
    byteY >>= 16;
    byteZ >>= 24;

    // now shift each byte to little endian position
    byteW <<= 24;
    byteX <<= 16;
    byteY <<= 8;
    byteZ <<= 0;

    // combine bytes
    num = (byteW | byteX | byteY | byteZ);
    return num;
}
