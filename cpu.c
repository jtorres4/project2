/*
 * Copyright (C) 2018 Gedare Bloom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu.h"
#include "memory.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

struct cpu_context cpu_ctx;

int fetch( struct IF_ID_buffer *out)
{
	//Get instruction
	printf("Fetched instruction - ");
	uint32_t fullInstruction = instruction_memory[(cpu_ctx.PC -  0x00400000)/4];
	printf("0x%x\n", fullInstruction);

	//Update program counter
	out->instruction = fullInstruction;
	out->pc = cpu_ctx.PC;
	out->next_pc = cpu_ctx.PC + 4;

	cpu_ctx.PC = out->next_pc;

	return 0;
}

int decode( struct IF_ID_buffer *in, struct ID_EX_buffer *out, struct control_unit *control )
{
	int fullInstruction = in->instruction;

	int opcode = fullInstruction & 0x0000007f;
	int registerDestination, regOne, regTwo; 
	int funct3 = 0;
	int funct7 = 0;
	int regOneData = 0;
	int regTwoData = 0;
	int immediate = 0;



	//Load Word (LW) Decode
	if(opcode == 3){
		registerDestination = (fullInstruction>>7)&0x1f;
		funct3 = (fullInstruction>>12)&0x7;
		regOne = (fullInstruction>>15)&0x1f;
		regOneData = cpu_ctx.GPR[regOne];
		immediate = (fullInstruction>>20)&0xfff;

		if((fullInstruction>>30)&0x1){
			int subtractMe = pow(2, 12);
			immediate = immediate - subtractMe;
		}
	}

	//Store Word (SW) Decode
	if(opcode == 35){
		int immTemp1 = (fullInstruction>>7)&0x1f;
		funct3 = (fullInstruction>>12)&0x7;

		regOne = (fullInstruction>>15)&0x1f;
		regOneData = cpu_ctx.GPR[regOne];
		//printf("Getting address to store to from %d and 0x%x \n", regOne, regOneData);

		regTwo = (fullInstruction>>20)&0x1f;
		regTwoData = cpu_ctx.GPR[regTwo];
		//printf("Getting data to Store from %d and STORING 0x%x \n", regTwo, regTwoData);

		int immTemp2 = (fullInstruction>>25)&0x7f;

		//Check last bit for sign
		if((fullInstruction>>30)&0x1){
			immediate = (immTemp2<<5) | immTemp1;
			int subtractMe = pow(2, 12);
			immediate = immediate - subtractMe;
		}
		else{
			immediate = immTemp2 + immTemp1;
		}

	}

	//Math/Logic I Type Decode
	if(opcode == 19){
		registerDestination = (fullInstruction>>7)&0x1f;
		funct3 = (fullInstruction>>12)&0x7;
		regOne = (fullInstruction>>15)&0x1f;
		regOneData = cpu_ctx.GPR[regOne];
		immediate = (fullInstruction>>20)&0xfff;
	}

	//jalr Decode
	if(opcode == 103){
		registerDestination = (fullInstruction>>7)&0x1f;
		funct3 = (fullInstruction>>12)&0x7;
		regOne = (fullInstruction>>15)&0x1f;
		regOneData = cpu_ctx.GPR[regOne];
		immediate = (fullInstruction>>20)&0xfff;
	}

	//Lui Decode
	if(opcode == 55){
		registerDestination = (fullInstruction>>7)&0x1f;
		immediate = (fullInstruction>>12);
		immediate = immediate << 12;
		//printf("Lui imm 0x%x \n", immediate);
	}

	//Auipc decode
	if(opcode == 23){
		registerDestination = (fullInstruction>>7)&0x1f;
		immediate = (fullInstruction>>12);
		immediate = (immediate << 12) + cpu_ctx.PC - 4;
		printf("AUIPC imm 0x%x and PC 0x%x \n", immediate, cpu_ctx.PC);
	}

	// All R type instructions decode
	if(opcode == 51){
		registerDestination = (fullInstruction>>7)&0x1f;
		funct3 = (fullInstruction>>12)&0x7;
		regOne = (fullInstruction>>15)&0x1f;
		regOneData = cpu_ctx.GPR[regOne];
		regTwo = (fullInstruction>>20)&0x1f;
		regTwoData = cpu_ctx.GPR[regTwo];
		//printf("reg two from: %d", regTwo);
		//printf("reg two data: %d", regTwoData);
		funct7 = (fullInstruction>>25)&0x7f;
	}

	//Branches SB type decode
	if(opcode == 99){
		int imm11, imm4t1, imm10t5, imm12;
		printf("Branch instr recieved!  ");

		funct3 = (fullInstruction>>12)&0x7;
		regOne = (fullInstruction>>15)&0x1f;
		regOneData = cpu_ctx.GPR[regOne];
		regTwo = (fullInstruction>>20)&0x1f;
		regTwoData = cpu_ctx.GPR[regTwo];

		imm11 = (fullInstruction>>7)&0x1;
		imm4t1 = (fullInstruction>>8)&0xf;
		imm10t5 = (fullInstruction>>25)&0x1f;
		imm12 = (fullInstruction>>31)&0x1;
		//printf("Imm12 %d, imm 11 %d, imm10t5 %d, imm 4t1 %d, imm0 0 \n", imm12, imm11, imm10t5, imm4t1);
		if(imm12 == 1){
			immediate = (imm12 << 11) | (imm11 << 10 )| (imm10t5 << 5) | (imm4t1 << 1) | 0x0;
			immediate = (~immediate) + 1; //Signed so make negative
			immediate = immediate & 0xfff; //Only 12 bits
			immediate = 0 - immediate;
		}
		else{
			immediate = (imm12 << 11) | (imm11 << 10 )| (imm10t5 << 5) | (imm4t1 << 1) | 0x0;
			}
		//printf("Immediate %d \n", immediate);
	}

	//Jal UJ decode
	if(opcode == 111){
		int imm19t12, imm11,imm10t1,imm20;

		registerDestination = (fullInstruction>>7)&0x1f;
		imm19t12 = (fullInstruction>>12)&0xff;
		imm11 = (fullInstruction>>20)&0x1;
		imm10t1 = (fullInstruction>>21)&0x3ff;
		imm20 = (fullInstruction>>31)&0x1;

		immediate = (imm20 << 19) | (imm19t12 << 11 )| (imm11 << 10) | (imm10t1 << 1) | 0x0;


		//printf("Immediate %d \n", immediate);
	}

	//Sign extend imm 
	int32_t sExtendImm = (int32_t)immediate;


	//Set control unit
	control->writeReg = registerDestination;

	//Type LW/SW control
	if(opcode == 3 || opcode == 35){
		control->ALUop0 = 0;
		control->ALUop1 = 0;
		control->ALUsrc = 1; //get imm from op 2
		control->PCsrc = 0;
		control->branch = 0;
		control->MemWrite = 0;
		control->MemRead = 0;
		control->MemToReg = 0;

		if(opcode == 35){control->MemWrite = 1;}
		if(opcode == 3){control->MemRead = 1; control->MemToReg = 1;}
	}



	//Type R/I -> ALUop1 depends on functs
	if(opcode == 51 || opcode == 19 || opcode == 23){
		//printf("Decoded R or I!");
		control->ALUop0 = 0;
		control->ALUop1 = 2;
		control->MemWrite = 0;
		control->MemRead = 0;
		control->MemToReg = 0;
		control->branch = 0;
		control->PCsrc = 0;

		if(opcode == 51){control->ALUsrc = 0;} //Use Reg from R type
		if(opcode == 19){control->ALUsrc = 1;} //Use Imm from I type
	}

	//Lui control
	if(opcode == 55){
		control->ALUsrc = 1;
		control->ALUop1 = 0;
		control->MemWrite = 0;
		control->MemRead = 0;
		control->MemToReg = 0;
		control->branch = 0;
		control->PCsrc = 0;
	}

	//Branching control
	if(opcode == 99){
		control->ALUop1 = 1;
		control->branch = 1;
		control->PCsrc = 0;
		control->MemWrite = 0;
		control->MemRead = 0;
		control->MemToReg = 0;	
		control->writeReg = 0;
	}

	//Jalr control
	if(opcode == 103){
		control->ALUop1 = 0;
		//control->branch = 1;
		control->PCsrc = 2;
		control->writeReg = 1;
		control->MemWrite = 0;
		control->MemRead = 0;
		control->MemToReg = 0;
	}

	//Jal control
	if(opcode == 111){
		control->ALUop1 = 0;
		//control->branch = 1;
		control->PCsrc = 1;
		control->writeReg = 1;
		control->MemWrite = 0;
		control->MemRead = 0;
		control->MemToReg = 0;
	}

	//printf("reg two %d",regTwoData);
	out->regOneData = regOneData;
	out->regTwoData = regTwoData;
	out->immediate = sExtendImm;
	out->funct3 = funct3;
	out->funct7 = funct7;

	//Ecall control and function
	if(opcode == 115){
		control->writeReg = 0;
		control->MemWrite = 0;
		control->MemRead = 0;
		control->MemToReg = 0;
		control->branch = 0;
		control->PCsrc = 0;
		printf("ECALL! - ");
		int a7 = cpu_ctx.GPR[17];
		if(a7 == 1){printf("OUTPUT INT %d \n", cpu_ctx.GPR[10]);} //a7 = 1 print int

		if(a7 == 4){
			printf("OUTPUT STRING: *starts at 0x%x* \n", cpu_ctx.GPR[10]);
			}
		
		if(a7 == 10){
			printf("Exit Call, returning %d \n", cpu_ctx.GPR[10]);
			exit(cpu_ctx.GPR[10]);
		}
	}

	/*
	if(opcode == 35){
		printf("regTwoData %x \n", regTwoData);
		out->regOneData = regTwoData;
	}*/

	return 0;
}

int execute( struct ID_EX_buffer *in, struct EX_MEM_buffer *out, struct control_unit *control, struct ALU_control *ALU_controler)
{
	int regOneData = in->regOneData;
	int regTwoData = in->regTwoData;
	int immediate = in->immediate;
	int funct3 = in->funct3;
	int funct7 = in->funct7;
	int secondData;
	//int ALUop2 = control->ALUop2;

	//Set ALU control
	ALUSet(funct3, funct7, control, ALU_controler);

	//printf("second data: %d", regTwoData);
	if(control->branch != 1)
		secondData = mux(regTwoData, immediate, control->ALUsrc);
	else 
		secondData = regTwoData;


	//printf("second data: %d", secondData);
	int result = runALU(regOneData, secondData, ALU_controler);

	//Branch check in Decode
	if(control->branch == 1){
		switch(funct3){
			case(0): //beq
				if(result == 0){
					printf("Beq - Branching to PC + %d \n", immediate);
					cpu_ctx.PC = cpu_ctx.PC + immediate - 4; //-4 is since PC was already updated by fetch
				}

				break;
			case(1):	//bne
				if(result != 0){
					printf("Bne - Branching to PC + %d \n", immediate);
					cpu_ctx.PC = cpu_ctx.PC + immediate - 4; //-4 is since PC was already updated by fetch
				}
				break;
			default: //
				printf("Unrecognized branch!");
				break;
		}
	}
	//Jal/Jalr
	if(control->PCsrc == 1){
			printf("Jal - Branching to PC + %d \n", immediate);
			result = cpu_ctx.PC + 4;
			cpu_ctx.PC = cpu_ctx.PC + immediate - 4;
	}if(control->PCsrc == 2){
			printf("Jalr - Branching to %d \n", result);
			int saveRA = cpu_ctx.PC + 4;
			cpu_ctx.PC = result;
			result = saveRA;
	}

	//Set for next cycle
	out->ALUresult = result;
	out->StoreData = regTwoData;

	return 0;
}

int memory( struct EX_MEM_buffer *in, struct MEM_WB_buffer *out, struct control_unit *control)
{
	int result = in->ALUresult;
	//printf("ALU result val %x !\n", result);
	int storeData = in->StoreData;

	//Not LW or SW
	if(control->MemRead == 0 && control->MemWrite == 0){
		printf("Not reading or storing memory!\n");
		printf("ALU result val %x !\n", result);
		out->resultToWB = result;
		return 0; //Not reading or storing
	}

	//Storing data SW
	if(control->MemWrite == 1){
		int address = result;
		printf("Storing %d in array index %d which is address %x \n", storeData, (address - 0x10000000)/ 4, address  );
		data_memory[(address - 0x10000000)/ 4] = storeData;
	}
	//LW
	if(control->MemRead == 1){
		int address = result;
		result = data_memory[(result - 0x10000000)/ 4];
		printf("Loading %d in array index %d which is address %x \n", result, (address - 0x10000000)/ 4, address  );
		out->resultToWB = result;
	}

	return 0;
}

int writeback( struct MEM_WB_buffer *in, struct control_unit *control)
{
	int writeBackData = in->resultToWB;

	//Normal writeback
	if(control->writeReg != 0 && control->MemToReg == 0){
		printf("Writing back %x to register x%d \n", writeBackData, control->writeReg);
		cpu_ctx.GPR[control->writeReg] = writeBackData;
	}
	
	//memory writeback
	if(control->MemToReg == 1){
		printf("Memory write back %x to register x%d \n", writeBackData, control->writeReg);
		cpu_ctx.GPR[control->writeReg] = writeBackData;
	}

	return 0;
}

void ALUSet(int funct3, int funct7, struct control_unit *controler, struct ALU_control *ALU_controler){
	
	switch (controler->ALUop1){
		case(0):	//LW/SW -> always add -> ALU op is 
			ALU_controler->ALUControl = 0b0010;
			break;
		case(1):	//Bne/Beq -> Always subtract
			ALU_controler->ALUControl = 0b0110;
			break;
		case(2): //R and I type -> depends on functs
			if (funct3 == 0 && funct7 == 0) //ADD
				ALU_controler->ALUControl = 0b0010;
			if (funct3 == 0 && funct7 != 0) //SUB
				//printf("Funct 7 %d", funct7);
				ALU_controler->ALUControl = 0b0110;
			if (funct3 == 1) //SLL
				ALU_controler->ALUControl = 0b0100;
			if (funct3 == 2) //SLT
				ALU_controler->ALUControl = 0b0111;
			if (funct3 == 4) //XOR
				ALU_controler->ALUControl = 0b0011;
			if (funct3 == 5 && funct7 == 0) //SRL
				ALU_controler->ALUControl = 0b0101;
			if (funct3 == 5 && funct7 != 0) //SRA
				ALU_controler->ALUControl = 0b1101;
			if (funct3 == 6) //OR
				ALU_controler->ALUControl = 0b0001;
			if (funct3 == 7) //AND
				ALU_controler->ALUControl = 0b0000;
			break;
		default:
			printf("Bad ALUop, %d, in ALU CONTROL!\n",controler->ALUop1 );
	}

	return;
}


int mux(int a, int b, int c){
	//Choses B in the presences of C, else A
	if(c) return b;
	return a;
}

int runALU(int dataOne, int dataTwo, struct ALU_control *ALU_controler){

	int result;
	char * ALUoperation;

	switch(ALU_controler->ALUControl){
		case(0b0000): //AND
			result = dataOne & dataTwo;
			ALUoperation = "AND";
			break;
		case(0b0001): //OR
			result = dataOne | dataTwo;
			ALUoperation = "OR";
			break;
		case(0b0010): //ADD
			result = dataOne + dataTwo;
			ALUoperation = "ADD";
			break;
		case(0b0011): //XOR
			result = dataOne ^ dataTwo;
			ALUoperation = "XOR";
			break;
		case(0b0100): //SLL
			result = dataOne << dataTwo;
			ALUoperation = "SLL";
			break;
		case(0b0101): //SRL
			result = dataOne >> dataTwo;
			ALUoperation = "SRL";
			break;
		case(0b1101): //SRA
			result = dataOne >> dataTwo;
			unsigned bigBit = 1;
			while (dataOne >>= 1) {bigBit++;}
			for(int x = 0; x < dataTwo; x++){
				result += (1 << bigBit - dataTwo + x);
			}
			ALUoperation = "SRA";
			break;
		case(0b0110): //SUB
			//printf("Data one: %d data Two %d ", dataOne, dataTwo);
			result = dataOne - dataTwo;
			ALUoperation = "SUB";
			break;
		case(0b0111): //SLT - result = 1 if A < B
			if((dataOne - dataTwo) < 0)
				result = 1;
			else
				result = 0;
			break;
		default:
			printf("Error in ALU!");
	}

	printf("ALU is returning %x using operation %s \n", result, ALUoperation);
	return result;
}


