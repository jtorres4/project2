#include "pipelined-cpu.h"
#include "memory.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

struct cpu_context cpu_ctx;
struct forward_unit forward;


struct control_unit fetch( struct IF_ID_buffer *out){
    struct control_unit result;

	//Get instruction
    printf("Fetched instruction - ");
	uint32_t fullInstruction = instruction_memory[(cpu_ctx.PC -  0x00400000)/4];
	printf("0x%x\n", fullInstruction);

	//Update program counter
	out->instruction = fullInstruction;
	out->pc = cpu_ctx.PC;
	out->next_pc = cpu_ctx.PC + 4;

	cpu_ctx.PC = out->next_pc;

    return result;
}

struct control_unit decode( struct IF_ID_buffer *in, struct ID_EX_buffer *out, struct control_unit *control){

    struct control_unit result;

    int fullInstruction = in->instruction;
	int opcode = fullInstruction & 0x0000007f;
	int registerDestination, regOne, regTwo; 
	int funct3 = 0;
    int funct7 = 0;
	int regOneData = 0;
	int regTwoData = 0;
	int immediate = 0;

    // R type decode
	if(opcode == 51){
		registerDestination = (fullInstruction>>7)&0x1f;
		funct3 = (fullInstruction>>12)&0x7;
		regOne = (fullInstruction>>15)&0x1f;
		regOneData = cpu_ctx.GPR[regOne];
		regTwo = (fullInstruction>>20)&0x1f;
        //printf("D-reg one from: %d\n", regOne);
		//printf("D-reg one data: %d\n", regOneData);
		regTwoData = cpu_ctx.GPR[regTwo];
		//printf("D-reg two from: %d\n", regTwo);
		//printf("D-reg two data: %d\n", regTwoData);
		funct7 = (fullInstruction>>25)&0x7f;
	}

    // I type Decode
    if(opcode == 19){
		registerDestination = (fullInstruction>>7)&0x1f;
		funct3 = (fullInstruction>>12)&0x7;
		regOne = (fullInstruction>>15)&0x1f;
		regOneData = cpu_ctx.GPR[regOne];
		immediate = (fullInstruction>>20)&0xfff;
	}

	//Load Word (LW)
	if(opcode == 3){
		registerDestination = (fullInstruction>>7)&0x1f;
		funct3 = (fullInstruction>>12)&0x7;
		regOne = (fullInstruction>>15)&0x1f;
		regOneData = cpu_ctx.GPR[regOne];
		immediate = (fullInstruction>>20)&0xfff;

		if((fullInstruction>>30)&0x1){
			int subtractMe = pow(2, 12);
			immediate = immediate - subtractMe;
		}
	}

	//Store Word (SW)
	if(opcode == 35){
		int immTemp1 = (fullInstruction>>7)&0x1f;
		funct3 = (fullInstruction>>12)&0x7;

		regOne = (fullInstruction>>15)&0x1f;
		regOneData = cpu_ctx.GPR[regOne];
		//printf("Getting address to store to from %d and 0x%x \n", regOne, regOneData);

		regTwo = (fullInstruction>>20)&0x1f;
		regTwoData = cpu_ctx.GPR[regTwo];
		//printf("Getting data to Store from %d and STORING 0x%x \n", regTwo, regTwoData);

		int immTemp2 = (fullInstruction>>25)&0x7f;

		//Check last bit for sign
		if((fullInstruction>>30)&0x1){
			immediate = (immTemp2<<5) | immTemp1;
			int subtractMe = pow(2, 12);
			immediate = immediate - subtractMe;
		}
		else{
			immediate = immTemp2 + immTemp1;
		}

	}

	//Jalr
	if(opcode == 103){
		registerDestination = (fullInstruction>>7)&0x1f;
		funct3 = (fullInstruction>>12)&0x7;
		regOne = (fullInstruction>>15)&0x1f;
		regOneData = cpu_ctx.GPR[regOne];
		//printf("JALR reg 1 %x", regOneData);
		immediate = (fullInstruction>>20)&0xfff;
	}

	//Lui
	if(opcode == 55){
		registerDestination = (fullInstruction>>7)&0x1f;
		immediate = (fullInstruction>>12);
		immediate = immediate << 12;
		//printf("Lui imm 0x%x \n", immediate);
	}

	//Auipc
	if(opcode == 23){
		registerDestination = (fullInstruction>>7)&0x1f;
		immediate = (fullInstruction>>12);
		immediate = (immediate << 12) + cpu_ctx.PC - 4;
		printf("AUIPC imm 0x%x and PC 0x%x \n", immediate, cpu_ctx.PC);
	}

	//Jal
	if(opcode == 111){
		int imm19t12, imm11,imm10t1,imm20;

		registerDestination = (fullInstruction>>7)&0x1f;
		imm19t12 = (fullInstruction>>12)&0xff;
		imm11 = (fullInstruction>>20)&0x1;
		imm10t1 = (fullInstruction>>21)&0x3ff;
		imm20 = (fullInstruction>>31)&0x1;

		immediate = (imm20 << 19) | (imm19t12 << 11 )| (imm11 << 10) | (imm10t1 << 1) | 0x0;


		//printf("Immediate %d \n", immediate);
	}

	//Branch 
	if(opcode == 99){
		int imm11, imm4t1, imm10t5, imm12;
		printf("Branch instr recieved!  ");

		funct3 = (fullInstruction>>12)&0x7;
		regOne = (fullInstruction>>15)&0x1f;
		regOneData = cpu_ctx.GPR[regOne];
		regTwo = (fullInstruction>>20)&0x1f;
		regTwoData = cpu_ctx.GPR[regTwo];

		imm11 = (fullInstruction>>7)&0x1;
		imm4t1 = (fullInstruction>>8)&0xf;
		imm10t5 = (fullInstruction>>25)&0x1f;
		imm12 = (fullInstruction>>31)&0x1;
		//printf("Imm12 %d, imm 11 %d, imm10t5 %d, imm 4t1 %d, imm0 0 \n", imm12, imm11, imm10t5, imm4t1);
		if(imm12 == 1){
			immediate = (imm12 << 11) | (imm11 << 10 )| (imm10t5 << 5) | (imm4t1 << 1) | 0x0;
			immediate = (~immediate) + 1; //Signed so make negative
			immediate = immediate & 0xfff; //Only 12 bits
			immediate = 0 - immediate;
		}
		else{
			immediate = (imm12 << 11) | (imm11 << 10 )| (imm10t5 << 5) | (imm4t1 << 1) | 0x0;
		}
		//printf("Immediate %d \n", immediate);
	}

	int32_t sExtendImm = (int32_t)immediate;

	//Branch Control
	if(opcode == 99){

		result.stall = 0;
		result.squash = 0;
        result.writeReg = 0;
        result.branch = 1;
        result.MemRead = 0;
		result.MemToReg = 0;
		result.ALUop0 = 0;
		result.ALUop1 = 1;
		result.MemWrite = 0;
		result.ALUsrc = 0;
		result.PCsrc = 0;
		result.decodeStall = 0;

		//Branch Decode forwarding
		if(regOne == forward.wbForwardReg && forward.wbForwardReg != 0){
        //printf("MemForwarding for regOne %d with value %d\n", forward.wbForwardReg, forward.wbForwardData);
        	regOneData = forward.wbForwardData;
    	} if(regTwo == forward.wbForwardReg && forward.wbForwardReg != 0){
        //printf("MemForwarding for regTwo %d with value %d\n", forward.wbForwardReg, forward.wbForwardData);
        	regTwoData = forward.wbForwardData;
    	}

		if(regOne == forward.memForwardReg && forward.memForwardReg != 0){
			//printf("MemForwarding for regOne %d with value %d\n", forward.memForwardReg, forward.memForwardData);
			regOneData = forward.memForwardData;
		}if(regTwo == forward.memForwardReg && forward.memForwardReg != 0){
			//printf("MemForwarding for regTwo %d with value %d\n", forward.memForwardReg, forward.memForwardData);
			regTwoData = forward.memForwardData;
		}

		if(regOne == forward.aluForwardReg && forward.aluForwardReg != 0){
			//printf("ALUForwarding for regOne %d with value %d\n", forward.aluForwardReg, forward.aluForwardData);
			regOneData = forward.aluForwardData;
			result.decodeStall = 1;
		} if(regTwo == forward.aluForwardReg && forward.aluForwardReg != 0){
			//printf("ALUForwarding for regTwo %d with value %d\n", forward.aluForwardReg, forward.aluForwardData);
			regTwoData = forward.aluForwardData;
			result.decodeStall = 1;
		}

		//Branch determined in decode
		int comparator = regOneData - regTwoData;
		switch(funct3){
			case(0): //beq
				if(comparator == 0){
					printf("Beq - Branching to PC + %d \n", immediate);
					result.squash = 1;
					cpu_ctx.PC = cpu_ctx.PC + immediate - 4; //-4 is since PC was already updated by fetch
				}
				break;
			case(1):	//bne
				if(comparator != 0){
					printf("Bne - Branching to PC + %d \n", immediate);
					result.squash = 1;
					cpu_ctx.PC = cpu_ctx.PC + immediate - 4; //-4 is since PC was already updated by fetch
				}
				break;
		default: //
			printf("Unrecognized branch!");
			break;
		}

	}

    // R and I and AUIPC Type Control
    if(opcode == 51 || opcode == 19 || opcode == 23){

		//printf("Decoded R or I!");
        result.stall = 0;
        result.squash = 0;
        result.writeReg = registerDestination;
        result.branch = 0;
        result.MemRead = 0;
		result.MemToReg = 0;
		result.ALUop0 = 0;
		result.ALUop1 = 2;
		result.MemWrite = 0;
        if(opcode == 51){result.ALUsrc = 0;} //Use Reg from R type
		if(opcode == 19 || opcode == 23){result.ALUsrc = 1;} //Use Imm from I type
		result.PCsrc = 0;
        result.ALUControl = 0;
		result.decodeStall = 0;

	}

	//LW and SW Control
	if(opcode == 3 || opcode == 35){
		result.stall = 0;
        result.squash = 0;
		result.writeReg = 0;
		result.branch = 0;
		result.ALUop1 = 0;
		result.ALUsrc = 1; //get imm from op 2
		result.PCsrc = 0;
        result.ALUControl = 0;
		result.MemWrite = 0;
		result.MemRead = 0;
		result.MemToReg = 0;
		result.decodeStall = 0;

		if(opcode == 35){result.MemWrite = 1;} //SW
		if(opcode == 3){
			result.MemRead = 1; 
			result.MemToReg = 1; 
			result.writeReg = registerDestination; 
		} //LW
	}

	//Lui Control
	if(opcode == 55){
		result.stall = 0;
        result.squash = 0;
        result.writeReg = registerDestination;
        result.branch = 0;
		result.ALUsrc = 1;
		result.ALUop1 = 0;
		result.MemWrite = 0;
		result.MemRead = 0;
		result.MemToReg = 0;
		result.branch = 0;
		result.PCsrc = 0;
		result.decodeStall = 0;
	}

	//Jalr Control
	if(opcode == 103){
		result.stall = 0;
        result.squash = 1;
		result.writeReg = 0;
		result.branch = 0;
		result.ALUop1 = 0;
		result.branch = 1;
		result.PCsrc = 2;
		result.ALUControl = 0;
		result.writeReg = 1;
		result.MemWrite = 0;
		result.MemRead = 0;
		result.MemToReg = 0;
		result.decodeStall = 0;

		printf("Jalr - Branching to %x \n", regOneData + sExtendImm);
		int saveRA = cpu_ctx.PC + 4;
		cpu_ctx.PC = regOneData + sExtendImm;
		regOneData = saveRA;
	}

	//Jal Control
	if(opcode == 111){
		result.stall = 0;
        result.squash = 1;
		result.branch = 0;
		result.ALUop1 = 0;
		result.branch = 1;
		result.PCsrc = 1;
		result.ALUControl = 0;
		result.writeReg = 1;
		result.MemWrite = 0;
		result.MemRead = 0;
		result.MemToReg = 0;
		result.decodeStall = 0;

		printf("Jal - Branching to PC + %d \n", sExtendImm);
		regOneData = cpu_ctx.PC + 4;
		cpu_ctx.PC = cpu_ctx.PC + sExtendImm;
		regTwoData = 0;

	}


    //All Ecall Functionality
    if(opcode == 115){

        result.stall = 0;
        result.squash = 0;
        result.writeReg = 0;
        result.branch = 0;
        result.MemRead = 0;
		result.MemToReg = 0;
		result.ALUop0 = 0;
		result.ALUop1 = 0;
		result.MemWrite = 0;
		result.ALUsrc = 0;
		result.PCsrc = 0;
        result.PCsrc = 0;

		printf("ECALL! - ");
		int a7 = cpu_ctx.GPR[17];
		if(a7 == 1){printf("OUTPUT INT %d \n", cpu_ctx.GPR[10]);} //a7 = 1 print int

		if(a7 == 4){
			printf("OUTPUT STRING: *starts at 0x%x* \n", cpu_ctx.GPR[10]);
			int temp = cpu_ctx.GPR[10];
			int i = 0;
			while (data_memory[(temp - 0x10000000)/4 + i] != '\0'){
				int charOne = data_memory[(temp - 0x10000000)/4 + i] & 0xff;
				int charTwo = data_memory[(temp - 0x10000000)/4 + i] & 0xff00;
				int charThree = data_memory[(temp - 0x10000000)/4 + i] & 0xff0000;
				int charFour = data_memory[(temp - 0x10000000)/4 + i] & 0xff000000;
				printf("%c%c%c%c", charOne, charTwo, charThree, charFour);
				i++;
			}
			printf("\n");
		}
		
		if(a7 == 10){
			printf("Exit Call, returning %d \n", cpu_ctx.GPR[10]);
			exit(cpu_ctx.GPR[10]);
		}
	}

    //Set out values
    out->regOne = regOne;
    out->regTwo = regTwo;
    out->regOneData = regOneData;
	out->regTwoData = regTwoData;
	out->immediate = sExtendImm;
	out->funct3 = funct3;
	out->funct7 = funct7;

    //printf("\nFirst data %d", out->regOneData);
    //printf("   Second Data %d\n", out->regTwoData);

    return result;
}

struct control_unit execute( struct ID_EX_buffer *in, struct EX_MEM_buffer *out, struct control_unit *control){
    struct control_unit result;

    result = *control; //Is this bad mojo?

    int regOne = in->regOne;
    int regTwo = in->regTwo;
    int regOneData = in->regOneData;
	int regTwoData = in->regTwoData;
	int immediate = in->immediate;
	int funct3 = in->funct3;
	int funct7 = in->funct7;
	int secondData;

	//Forwarding
	if(regOne == forward.wbForwardReg && forward.wbForwardReg != 0){
        //printf("MemForwarding for regOne %d with value %d\n", forward.wbForwardReg, forward.wbForwardData);
        regOneData = forward.wbForwardData;
    } if(regTwo == forward.wbForwardReg && forward.wbForwardReg != 0){
        //printf("MemForwarding for regTwo %d with value %d\n", forward.wbForwardReg, forward.wbForwardData);
        regTwoData = forward.wbForwardData;
    }

	if(regOne == forward.memForwardReg && forward.memForwardReg != 0){
        //printf("MemForwarding for regOne %d with value %d\n", forward.memForwardReg, forward.memForwardData);
		if(forward.loadUseHazard == 1){result.loadUseStall = 1;}
        regOneData = forward.memForwardData;
    } if(regTwo == forward.memForwardReg && forward.memForwardReg != 0){
        //printf("MemForwarding for regTwo %d with value %d\n", forward.memForwardReg, forward.memForwardData);
		if(forward.loadUseHazard == 1){result.loadUseStall = 1;}
        regTwoData = forward.memForwardData;
    }

    if(regOne == forward.aluForwardReg && forward.aluForwardReg != 0){
        //printf("ALUForwarding for regOne %d with value %d\n", forward.aluForwardReg, forward.aluForwardData);
        regOneData = forward.aluForwardData;
    } if(regTwo == forward.aluForwardReg && forward.aluForwardReg != 0){
        //printf("ALUForwarding for regTwo %d with value %d\n", forward.aluForwardReg, forward.aluForwardData);
        regTwoData = forward.aluForwardData;
    }

    //printf("In forwarding, MemReg %d, MemVal %d   ", forward.memForwardReg, forward.memForwardData);
    //printf("   ALUReg %d, ALUVal %d", forward.memForwardReg, forward.memForwardData);

	//Get ALU control signal
    control->ALUControl = ALUSet(funct3, funct7,control);

	//Get second value based on imm or not
    if(control->branch != 1)
		secondData = mux(regTwoData, immediate, control->ALUsrc);
	else 
		secondData = regTwoData;

    //printf("\nX-First data %x", regOneData);
    //printf("\nX-Second Data %x\n", secondData);

	//Run ALU
    int aluResult = runALU(regOneData, secondData, control->ALUControl);

	//Print ALU Result
	printf("aluResult: %x ", aluResult);
    out->ALUresult = aluResult;
	//printf("regTwoData: %x ", regTwoData);
	out->StoreData = regTwoData;

	//Forward if not SW or LW
	if(result.MemRead != 1 && result.MemWrite != 1){
    	forward.aluForwardReg = result.writeReg;
    	forward.aluForwardData = aluResult;
	} else{
		forward.aluForwardReg = 0;
		forward.aluForwardData = 0;
	}

    //printf("Forwarding val %d from reg %d to unit\n", forward.aluForwardData, forward.aluForwardReg);

    return result;
}

struct control_unit memory( struct EX_MEM_buffer *in, struct MEM_WB_buffer *out, struct control_unit *control){
    struct control_unit result;

    result = *control;

	int ALUresult = in->ALUresult;
	int storeData = in->StoreData;

	//Not LW or SW
	if(control->MemRead == 0 && control->MemWrite == 0){
		//printf("Not reading or storing memory!\n");
		out->resultToWB = ALUresult;
	}

	//Storing data SW
	if(control->MemWrite == 1){
		int address = ALUresult;
		printf("Storing %d in array index %d which is address %x \n", storeData, (address - 0x10000000)/ 4, address  );
		data_memory[(address - 0x10000000)/ 4] = storeData;
	}
	//Loading LW
	if(control->MemRead == 1){
		int address = ALUresult;
		ALUresult = data_memory[(ALUresult - 0x10000000)/ 4];
		printf("Loading %d in array index %d which is address %x \n", ALUresult, (address - 0x10000000)/ 4, address  );
		out->resultToWB = ALUresult;
	}

	//Forward results
    forward.memForwardReg = result.writeReg;
    forward.memForwardData = ALUresult;
	forward.loadUseHazard = 0;
	//Check for Load Use Hazard
	if(control->MemRead == 1){
		//printf("Potential LW hazard");
		forward.loadUseHazard = 1;
		//printf("\nMemForwarding val %d from reg %d to unit\n", forward.memForwardData, forward.memForwardReg);
	}

    //printf("MemForwarding val %d from reg %d to unit\n", forward.memForwardData, forward.memForwardReg);

    return result;
}

int writeback( struct MEM_WB_buffer *in, struct control_unit *control ){

    int writeBackData = in->resultToWB;

	//Forward - technically redundant based on WB executing before decode instead of simultaneously
    forward.wbForwardReg = control->writeReg;
    forward.wbForwardData = writeBackData;

	//Normal Writeback
	if(control->writeReg != 0 && control->MemToReg == 0){
		printf("Writing back 0x%x to register x%d \n", writeBackData, control->writeReg);
		cpu_ctx.GPR[control->writeReg] = writeBackData;
	}

	//LW writeback
	if(control->MemToReg == 1){
		printf("Memory write back 0x%x to register x%d \n", writeBackData, control->writeReg);
		cpu_ctx.GPR[control->writeReg] = writeBackData;
	}


    return 0;
}


int ALUSet(int funct3, int funct7, struct control_unit *controler){

    int result = 2;
    //printf("ALU_op1 - %d", controler->ALUop1);
    //printf("Funct 7 - %d", controler->ALUop1);

    switch (controler->ALUop1){
		case(0):	//LW/SW -> always add -> ALU op is 
			result = 0b0010;
			break;
		case(1):	//Bne/Beq -> Always subtract
			result = 0b0110;
			break;
		case(2): //R and I type -> depends on functs
			if (funct3 == 0 && funct7 == 0) //ADD
				result = 0b0010;
			if (funct3 == 0 && funct7 != 0) //SUB
				//printf("Funct 7 %d", funct7);
				result = 0b0110;
			if (funct3 == 1) //SLL
				result = 0b0100;
			if (funct3 == 2) //SLT
				result = 0b0111;
			if (funct3 == 4) //XOR
				result = 0b0011;
			if (funct3 == 5 && funct7 == 0) //SRL
				result = 0b0101;
			if (funct3 == 5 && funct7 != 0) //SRA
				result = 0b1101;
			if (funct3 == 6) //OR
				result = 0b0001;
			if (funct3 == 7) //AND
				result = 0b0000;
			break;
		default:
			printf("Bad ALUop, %d, in ALU CONTROL!\n",controler->ALUop1 );
	}

    return result;
}

int runALU(int dataOne, int dataTwo, int ALUControl){

    int result;
	char * ALUoperation;

	switch(ALUControl){
		case(0b0000): //AND
			result = dataOne & dataTwo;
			ALUoperation = "AND";
			break;
		case(0b0001): //OR
			result = dataOne | dataTwo;
			ALUoperation = "OR";
			break;
		case(0b0010): //ADD
			result = dataOne + dataTwo;
			ALUoperation = "ADD";
			break;
		case(0b0011): //XOR
			result = dataOne ^ dataTwo;
			ALUoperation = "XOR";
			break;
		case(0b0100): //SLL
			result = dataOne << dataTwo;
			ALUoperation = "SLL";
			break;
		case(0b0101): //SRL
			result = dataOne >> dataTwo;
			ALUoperation = "SRL";
			break;
		case(0b1101): //SRA
			result = dataOne >> dataTwo;
			unsigned bigBit = 1;
			while (dataOne >>= 1) {bigBit++;}
			for(int x = 0; x < dataTwo; x++){
				result += (1 << bigBit - dataTwo + x);
			}
			ALUoperation = "SRA";
			break;
		case(0b0110): //SUB
			//printf("Data one: %d data Two %d ", dataOne, dataTwo);
			result = dataOne - dataTwo;
			ALUoperation = "SUB";
			break;
		case(0b0111): //SLT - result = 1 if A < B
			if((dataOne - dataTwo) < 0)
				result = 1;
			else
				result = 0;
			break;
		default:
			printf("Error in ALU!");
	}

	printf("ALU is returning %x using operation %s \n", result, ALUoperation);
	return result;
}

int mux(int a, int b, int c){
    //Choses B in the presences of C, else A
    if(c) return b;
	return a;
}
